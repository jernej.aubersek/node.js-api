import * as express from 'express'
import * as mongoose from 'mongoose'
import loginController from './controllers/LoginController'
import userController from './controllers/UserController'
import { ResponseError, AuthError } from './bussiness/ResponseError'

class App {
    public express

    constructor () {
        this.express = express()
        this.connectDb()
        this.mountRoutes()
    }

    mountRoutes(): void {
        this.express.use(express.json())
        const router = express.Router()
        this.express.use('/', router)
        this.express.use('/', loginController)
        this.express.use('/', userController)

        this.express.use((err, req, res, next) => {
            if (err instanceof AuthError) {
                res.status(err.status).json({ auth: false, message: err.message })
            }
            if(err instanceof ResponseError) {
                res.status(err.status).json({ message: err.message })
            }
            next(err)
        })
    }

    responseErrorHandler (err, req, res, next) {
        if(err instanceof ResponseError) {
            res.status(err.status).json({ message: err.message })
        }
        next(err)
    }

    connectDb() {
        var mongoDB = 'mongodb://aubersek:abc123@ds159624.mlab.com:59624/povio-api';
        mongoose.connect(mongoDB);
        var db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    }
}

export default new App().express 