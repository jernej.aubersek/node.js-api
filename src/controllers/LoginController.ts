import * as express from 'express'
import * as login from '../bussiness/login'
import { catchErrorsMiddleware } from './controllerHelper'


export class LoginController {
    public router

    constructor() {
        this.router = express.Router()
        this.defineRoutes()
    }

    defineRoutes(): void {
        this.router.post('/signup', catchErrorsMiddleware(async (req, res) => {
            let authToken = await login.signUp(req.body.username, req.body.password)
            res.json({ auth: true, token: authToken })
        }))
        this.router.post('/login', catchErrorsMiddleware(async (req, res) => {
            let authToken = await login.login(req.body.username, req.body.password)
            res.json({ auth: true, token: authToken })
        }))
        this.router.get('/me', login.verifyToken, catchErrorsMiddleware(async (req, res) => {
            let userInfo = await login.getLoggedUser(req.userId)
            res.json(userInfo)
        }))
        this.router.put('/me/update-password', login.verifyToken, catchErrorsMiddleware(async (req, res) => {
            await login.updatePassword(req.userId, req.body.password)
            res.json({ message: 'Password updated' })
        }))
    }
}

export default new LoginController().router