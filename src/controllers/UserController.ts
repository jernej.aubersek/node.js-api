import * as express from 'express'
import * as login from '../bussiness/login'
import * as userLikes from '../bussiness/userLikes'
import { catchErrorsMiddleware } from './controllerHelper'

class UserController {
    public router

    constructor() {
        this.router = express.Router()
        this.defineRoutes()
    }

    defineRoutes(): void {
        this.router.get('/user/:id', catchErrorsMiddleware(async (req, res) => {
            let userWithLikes = await userLikes.getUserWithLikes(req.params.id)
            res.json(userWithLikes)
        }))
        this.router.put('/user/:id/like', login.verifyToken, catchErrorsMiddleware(async (req, res) => {
            let likedUsername = await userLikes.like(req.userId, req.params.id)
            res.json( { liked: likedUsername } )
        }))
        this.router.delete('/user/:id/unlike', login.verifyToken, catchErrorsMiddleware(async (req, res) => {
            let unlikedUsername = await userLikes.unlike(req.userId, req.params.id)
            res.json( { unliked: unlikedUsername } )
        }))
        this.router.get('/most-liked', catchErrorsMiddleware(async (req, res) => {
            let mostLikedList = await userLikes.listOfUsersByLikes()
            res.json(mostLikedList)
        }))
    }
}

export default new UserController().router