import { Document, Schema, Model, model} from "mongoose";
import { IUser } from "../models/IUser";

export interface IUserModel extends IUser, Document { }

export var userSchema: Schema = new Schema({
  username: { type: String, required: true, unique: true},
  password: { type: String, required: true },
  liked: [{
    username: { type: String, required: true }
  }]
});

export const User: Model<IUserModel> = model<IUserModel>("User", userSchema);