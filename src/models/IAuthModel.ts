export interface IAuthModel {
    auth: boolean
    token?: string
    message?: string
}