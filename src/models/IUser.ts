export interface IUser {
    username: string
    password?: string
    liked?: [ IUser ]
}