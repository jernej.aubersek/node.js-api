export interface IUserWithLikes {
    user: string
    likes: number
}