export interface ILoggedUser {
    username: string
    liked: {
        username: string
    }[]
}