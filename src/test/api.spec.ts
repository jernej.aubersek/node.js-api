import * as request from 'supertest'
import * as sinon from 'sinon'
import * as assert from 'assert';
import * as jwt from 'jsonwebtoken'
import app from '../App'
import * as login from '../bussiness/login'
import * as userLikes from '../bussiness/userLikes'
import { AuthError, ResponseError } from '../bussiness/ResponseError';

/*
This test assignment will have you build an application with the following REST endpoints
/signup
Sign up to the system (username, password)
/login
Logs in an existing user with a password
!/me
Get the currently logged in user information
!/me/update-password
Update the current users password
/user/:id/
List username & number of likes of a user
!/user/:id/like
Like a user
!/user/:id/unlike	
Un-Like a user
/most-liked
List users in a most liked to least liked
Each user can like another only once, and they can unlike eachother.
The bolded endpoints are authenticated calls.
Select the appropriate REST calls (get, put,post, delete) by yourself.
*/

before(done => {
  let jwtStub = sinon.stub(jwt)

  jwtStub.verify.withArgs('avalidtoken', 'supersecret').resolves({ id: 'john' })
  jwtStub.verify.withArgs('invalidtoken', 'supersecret').throws(new AuthError('wrong token', 500))

  let loginStub = sinon.stub(login)

  loginStub.signUp.withArgs('john', sinon.match.any).resolves('avalidtoken')
  loginStub.signUp.withArgs('marc', sinon.match.any).throws(new AuthError('wrong password', 500))

  loginStub.login.withArgs('john', 'snow').resolves('avalidtoken')
  loginStub.login.withArgs('john', 'stark').throws(new AuthError('wrong password', 500))
  loginStub.login.withArgs('marc', sinon.match.any).throws(new AuthError('no user', 500))

  loginStub.getLoggedUser.withArgs('john').resolves({ username: 'john', liked: [] })

  let userLikesStub = sinon.stub(userLikes)

  userLikesStub.getUserWithLikes.withArgs('john').resolves({ user: 'john', likes: 12 })
  userLikesStub.getUserWithLikes.withArgs('marc').throws(new ResponseError('user not found', 400))

  userLikesStub.like.withArgs('john', 'marc').resolves('marc')
  userLikesStub.like.withArgs('john', 'stef').throws(new ResponseError('already liked', 500))
  userLikesStub.like.withArgs('john', 'john').throws(new ResponseError('cannot like itself', 400))
  userLikesStub.like.withArgs('john', 'rob').throws(new ResponseError('user not found', 400))

  userLikesStub.unlike.withArgs('john', 'marc').resolves('marc')
  userLikesStub.unlike.withArgs('john', 'rob').throws(new ResponseError('user not found', 400))

  userLikesStub.listOfUsersByLikes.resolves([{user: 'john', likes: 2}])

  done()
})

describe('api /signup', () => {
  it('signs up the new user', done => {
    request(app)
      .post('/signup')
      .send({ username: 'john', password: 'snow' })
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, true)
        assert.equal(res.body.token, 'avalidtoken')
        done()
      })
  })

  it('rejects sign up - user exists', done => {
    request(app)
      .post('/signup')
      .send({ username: 'marc', password: 'snow' })
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        assert.equal(res.body.message, 'wrong password')
        done()
      })
  })
})

describe('api /login', () => {
  it('logs the user in', done => {
    request(app)
      .post('/login')
      .send({ username: 'john', password: 'snow' })
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, true)
        assert.equal(res.body.token, 'avalidtoken')
        done()
      })
  })

  it('rejects login - user does not exist', done => {
    request(app)
      .post('/login')
      .send({ username: 'john', password: 'stark' })
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        assert.equal(res.body.message, 'wrong password')
        done()
      })
  })

  it('rejects login - wrong password', done => {
    request(app)
      .post('/login')
      .send({ username: 'marc', password: 'stark' })
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        assert.equal(res.body.message, 'no user')
        done()
      })
  })
})

describe('api /me', () => {
  it('shows loged user data', done => {
    request(app)
      .get('/me')
      .set('x-access-token', 'avalidtoken')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.username, 'john')
        done()
      })
  })

  it('no current user - request sent without token', done => {
    request(app)
      .get('/me')
      .expect(403)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })

  it('no current user - wrong token', done => {
    request(app)
      .get('/me')
      .set('x-access-token', 'invalidtoken')
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })
})

describe('api /me/update-password', () => {
  it('updates password', done => {
    request(app)
    .put('/me/update-password')
    .set('x-access-token', 'avalidtoken')
    .send({ password: 'stark' })
    .expect(200)
    .end((err, res) => {
      if (err) {
        return done(err)
      }
      assert.equal(res.body.message, 'Password updated')
      done()
    })
  })

  it('password update fails - request sent without token', done => {
    request(app)
      .put('/me/update-password')
      .expect(403)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })

  it('password update fails - wrong token', done => {
    request(app)
      .put('/me/update-password')
      .set('x-access-token', 'invalidtoken')
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })
})

describe('api /user/:id/', () => {
  it('shows user data', done => {
    request(app)
      .get('/user/john')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.user, 'john')
        assert.equal(res.body.likes, 12)
        done()
      })
  })

  it('no user with that id exists', done => {
    request(app)
      .get('/user/marc')
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.message, 'user not found')
        done()
      })
  })
})

describe('api /user/:id/like', () => {
  it('adds like to user', done => {
    request(app)
      .put('/user/marc/like')
      .set('x-access-token', 'avalidtoken')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.liked, 'marc')
        done()
      })
  })

  it('user already liked', done => {
    request(app)
      .put('/user/stef/like')
      .set('x-access-token', 'avalidtoken')
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.message, 'already liked')
        done()
      })
  })

  it('user cannot like itself', done => {
    request(app)
      .put('/user/john/like')
      .set('x-access-token', 'avalidtoken')
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.message, 'cannot like itself')
        done()
      })
  })

  it('no user with that id exists', done => {
    request(app)
      .put('/user/rob/like')
      .set('x-access-token', 'avalidtoken')
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.message, 'user not found')
        done()
      })
  })

  it('request sent without token', done => {
    request(app)
      .put('/user/john/like')
      .expect(403)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })

  it('wrong token', done => {
    request(app)
      .put('/user/john/like')
      .set('x-access-token', 'invalidtoken')
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })
})

describe('api /user/:id/unlike', () => {
  it('unlikes user', done => {
    request(app)
      .delete('/user/marc/unlike')
      .set('x-access-token', 'avalidtoken')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.unliked, 'marc')
        done()
      })
  })

  it('no user with that id exists', done => {
    request(app)
      .delete('/user/rob/unlike')
      .set('x-access-token', 'avalidtoken')
      .expect(400)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.message, 'user not found')
        done()
      })
  })

  it('request sent without token', done => {
    request(app)
      .delete('/user/john/unlike')
      .expect(403)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })

  it('wrong token', done => {
    request(app)
      .delete('/user/john/unlike')
      .set('x-access-token', 'invalidtoken')
      .expect(500)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body.auth, false)
        done()
      })
  })
})

describe('api /most-liked', () => {
  it('returns most liked users', done => {
    request(app)
      .get('/most-liked')
      .expect(200)
      .end((err, res) => {
        if (err) {
          return done(err)
        }
        assert.equal(res.body[0].user, 'john')
        assert.equal(res.body[0].likes, 2)
        done()
      })
  })
})