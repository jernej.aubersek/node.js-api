export class ResponseError extends Error {
    public status: number

    constructor(message: string, httpStatus: number) {
        super(message)
        this.name = "ResponseError"
        this.status = httpStatus
    }
}

export class AuthError extends ResponseError {
    constructor(message: string, httpStatus: number) {
        super(message, httpStatus)
        this.name = "AuthError"
    }
}