import { IUserWithLikes } from '../models/IUserWithLikes'
import { User, IUserModel } from '../db/user'
import { ResponseError } from './ResponseError'
import { IUser } from '../models/IUser'

export async function getUserWithLikes(username: string): Promise<IUserWithLikes> {
    let user: IUserModel = await findUser(username)
    let numberOfLikes = await User.aggregate([{ $unwind: '$liked' },
                                              { $match: { 'liked._id': user._id } },
                                              { $count: 'likes' }])

    return {
        user: user.username,
        likes: numberOfLikes.length > 0 ? numberOfLikes.pop().likes : 0
    }
}

export async function like(loggedId: string, username: string): Promise<string> {
    let loggedUser: IUserModel = await User.findById(loggedId)
    let likedUser: IUserModel = await findUser(username)
    if (likedUser.equals(loggedUser)) {
        throw new ResponseError('User cannot like itself', 400)
    }
    let likedUsers: IUser = loggedUser.toObject()
    if (likedUsers.liked.findIndex(i => i.username === username) === -1) {
        await User.update(loggedUser, { $push: { liked: likedUser } })
        return username
    }
    throw new ResponseError(`User ${username} already liked`, 500)
}

export async function unlike(loggedId: string, username: string): Promise<string> {
    let loggedUser: IUserModel = await User.findById(loggedId)
    await pullFromLikedList(loggedUser, username)
    return username
}

export async function listOfUsersByLikes() {
    let result = await User.aggregate([{ $unwind: '$liked' },
                                       { $group: { _id: {  _id: '$liked._id', username: '$liked.username' }, likes: { $sum: 1 } } },
                                       { $project: { _id: 0, user: '$_id.username', likes: 1 } },
                                       { $sort: { likes: -1 } }])
    return result
}

async function pullFromLikedList(loggedUser: IUserModel, username: string): Promise<IUserModel> {
    let unlikedUser: IUserModel = await findUser(username)
    await User.update(loggedUser, { $pull: { liked: unlikedUser } })
    return unlikedUser
}

async function findUser(username: string): Promise<IUserModel> {
    let user: IUserModel = await User.findOne({ username: username })
    if (!user) {
        throw new ResponseError(`User ${username} cannot be found`, 400)
    }
    return user
}