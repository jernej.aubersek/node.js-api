import * as jwt from 'jsonwebtoken'
import * as bcrypt from 'bcrypt'
import { User, IUserModel } from '../db/user'
import { Request, Response, NextFunction } from 'express'
import { ILoggedUser } from '../models/ILoggedUser'
import { AuthError, ResponseError } from './ResponseError';

const tokenSecret = 'supersecret'
const saltRounds = 10

export async function signUp(username: string, password: string): Promise<string> {
    if (!username || !password) {
        throw new AuthError ('No username or password provided', 400)
    }
    password = await bcrypt.hash(password, saltRounds)
    let newUser = await User.create({ username: username, password: password })
    return generateToken(newUser)
}

export async function login(username: string, password: string): Promise<string> {
    if (!username || !password) {
        throw new AuthError('No username or password provided', 400)
    }
    let foundUser = await User.findOne({username: username})
    if (foundUser) {
        if (await bcrypt.compare(password, foundUser.password)) {
            return generateToken(foundUser)
        }
        throw new AuthError('Wrong password', 500)
    }
    throw new AuthError('User does not exist', 500)
}

export function generateToken(user: IUserModel): string {
    return jwt.sign({ id: user._id }, tokenSecret, {
        expiresIn: '1h'
    })
}

export function verifyToken(req: Request, res: Response, next: NextFunction) {
    var token = req.headers['x-access-token']
    if (!token) {
        throw new AuthError('No token provided', 403)
    }

    try {
        jwt.verify(token, tokenSecret)
        .then(decoded => {
            req.userId = decoded.id
            next()
        })
    } catch {
        throw new AuthError('Token authentication failed', 500)
    }
}

export async function updatePassword(id: String, password: string): Promise<void> {
    if (!password) {
        throw new ResponseError('New password not provided', 400)
    }
    try {
        let passwordHash = await bcrypt.hash(password, saltRounds)
        await User.findByIdAndUpdate(id, { password: passwordHash })
    } catch {
        throw new ResponseError('Cannot update password', 500)
    }
}

export async function getLoggedUser(id: String): Promise<ILoggedUser> {
    var user = await User.findById(id)
    return {
        username: user.username,
        liked: user.liked.map(val => {
            return { username: val.username }
        })
    }
}